import Channel from "../models/Channel";
import Post from "../models/Post";
import User from "../models/User";
import Comment from "../models/Comment";

class ChannelController{

    /**
     * Create channel in database
     * @param {*} req 
     * @param {*} res 
     */
    static async create(req, res){
        let status = 200;
        let body = {};


        try{

            let slug = req.body.name.replace(/[-\s ]+/,'-').toLowerCase();

            let channel = await Channel.create({
               name: req.body.name,
               ownerId: req.body.ownerId,
               slug: slug,
               avatar: req.file && `uploads/${req.file.filename}`
            });

            body = {
                channel,
                'message': 'Channel created'
            };

        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }

        return res.status(status).json(body);
    }
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let channels = await Channel.find().populate('ownerId');

            body = {
                channels,
                'message' : 'Channel list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }


    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let id = req.params.id;
            let channel = await Channel.findById(id).populate('ownerId');

            let posts = await Post.find({channelId: id}).sort('-date').populate('userId');

            let comments = [];
            for(let i in posts){
                comments.push(await Comment.countDocuments({postId: posts[i]._id}));
            }
            
            body = {
                comments,
                channel,
                posts,
                'message' : 'Channel detail'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try{
            
            await Channel.deleteOne({_id : req.params.id});

            body = {
                'message' : 'Channel deleted'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }
    
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            if(req.body.name){
                let slug = req.body.name.replace(/[-\s ]+/g,'-').toLowerCase();
                req.body.slug = slug;
            }
            if(req.file){
                req.body.avatar = `uploads/${req.file.filename}`
            }
            

            await Channel.findByIdAndUpdate(req.params.id, {$set : req.body});
            let channel = await Channel.findById(req.params.id);

            body = {
                'body': req.body,
                channel,
                'message' : 'Channel updated'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }


    // Liste des administrateurs du channel
    static async listAdmins(req, res){
        let status = 200;
        let body = {};

        try{
            let id = req.params.id;
            let channel = await Channel.findById(id);
            let owner = channel.ownerId;


            let admins = [];
            for(let i in channel.adminsId){
                admins.push(await User.findById({_id: channel.adminsId[i]}));
            }
            
            body = {
                owner,
                admins,
                'message' : 'Channel admins list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }
    
    // Ajout d'un administrateur au channel
    static async adminAdd(req, res){
        let status = 200;
        let body = {};

        try{
            let channel = await Channel.findById(req.params.id);  
            let isAdmin = channel.adminsId.indexOf(req.body.adminId);
            let message = '';

            if(isAdmin === -1){
                await Channel.findByIdAndUpdate(req.params.id, { $push: { 'adminsId': req.body.adminId } });
                message = 'Channel updated';
            }else{
                message = 'The user is already an admin';
            }
            

            body = {
                'message' : message
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    // Suppression d'un administrateur
    static async adminDelete(req, res){
        let status = 200;
        let body = {};

        try{
            await Channel.findByIdAndUpdate(req.params.id, { $pull: { 'adminsId': req.body.adminId } });
            body = {
                'message' : 'Admin deleted'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    // Liste des abonnées du channel
    static async listSubsribers(req, res){
        let status = 200;
        let body = {};

        try{
            let id = req.params.id;
            let channel = await Channel.findById(id);
            let owner = channel.ownerId;


            let subscribers = [];
            for(let i in channel.subscribersId){
                subscribers.push(await User.findById({_id: channel.subscribersId[i]}));
            }
            
            body = {
                owner,
                subscribers,
                'message' : 'Channel subscribers list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    // Ajout d'un abonné
    static async subscriberAdd(req, res){
        let status = 200;
        let body = {};

        try{
            let channel = await Channel.findById(req.params.id);  
            let isSubscriber = channel.subscribersId.indexOf(req.body.subscriberId);

            let message = '';

            if(isSubscriber === -1){
                await Channel.findByIdAndUpdate(req.params.id, { $push: { 'subscribersId': req.body.subscriberId } });
                message = 'Subscriber addred';
            }else{
                message = 'The user is already a subcriber';
            }
            

            body = {
                'id':req.params.id,
                'body': req.body,
                'message' : message
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    // Suppression d'un abonnée
    static async subscriberDelete(req, res){
        let status = 200;
        let body = {};

        try{
            await Channel.findByIdAndUpdate(req.params.id, { $pull: { 'subscribersId': req.body.subscriberId } });
            body = {
                'message' : 'Subscriber deleted'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    
}


export default ChannelController;