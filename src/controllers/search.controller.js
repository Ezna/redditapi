import Channel from "../models/Channel";
import Post from "../models/Post";
import User from "../models/User";

class SearchController{

    /**
     * Search any in database
     * @param {*} req 
     * @param {*} res 
     */
  

    // Recherche dans les channels, articles, utilisateurs avec un mot clé
    static async search(req, res){
        let status = 200;
        let body = {};

        try{
            let regex = new RegExp(`.*${req.params.keyword}*.`,'i');
            let channels = await Channel.find({name : {$regex : regex}}).populate('ownerId');
            let posts = await Post.find({title : {$regex : regex}}).populate('userId channelId');
            let users = await User.find({nickname : {$regex : regex}});

            body = {
                channels,
                posts,
                users,
                'message' : 'Search result'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }
    
    
}


export default SearchController;