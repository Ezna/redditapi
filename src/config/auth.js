import jsonwebTokken from 'jsonwebtoken';
import User from '../models/User';

class Auth{
    /**
     * Check if user has rright to acces route
     * @param {Array} roles 
     */
    static auth(roles){
        return async(req, res, next) => {
            try {
                let token = req.headers.authorization.replace(/Bearer /g, '');
                let decryptoken = jsonwebTokken.decode(token,'monsecret');

                let user = await User.findById(decryptoken.sub);

                if(user && user.user_role == 10){
                    next();
                }else if(user && roles.includes(user.user_role)){
                    next();
                }else{
                    res.status(401).json({'message' : 'Unauthorized'});
                }

            } catch (error) {
                res.status(403).json({'message' : 'Error'});
            }
        }
    }
}

export default Auth;