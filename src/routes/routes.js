import { Router } from 'express';
import UserController from '../controllers/user.controller';
import PostController from '../controllers/post.controller';
import ChannelController from '../controllers/channel.controller';
import CommentController from '../controllers/comment.controller';
import SearchController from '../controllers/search.controller';
import Auth from '../config/auth';
import ReportController from '../controllers/report.controller';

const router = Router();
// Réception des données multiparts pour les images
const multer = require('multer');
// Stockage des images dans le fichier upload à la racine
const storage = multer.diskStorage({
    destination :  (req, file, cb) => {
        cb(null,'./uploads')
    },
    filename : (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, `${uniqueSuffix}-${file.originalname}`);
    }
});
// Filtre sur les types de données récupérer, uniquement les images
const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true);
    }else{
        cb(null,false);
    }
}

// Init
const upload = multer({
    storage: storage,
    fileFilter : fileFilter
});

// Routes
// GET, POST, PUT, DELETE, PATCH


// Users routes

router.post('/users/authentificate', UserController.auth);
router.get('/users', UserController.list);
router.post('/users', upload.single('avatar'), UserController.create);
router.get('/users/:id', UserController.details);
router.delete('/users/:id', Auth.auth([10]), UserController.delete);
router.put('/users/:id',upload.single('avatar'), Auth.auth([1]), UserController.update);
router.get('/users/subscriptions/:id', Auth.auth([1]), UserController.listSubscriptions);

router.put('/users/ban/:id', Auth.auth([10]), UserController.ban);

router.get('/channels', ChannelController.list);
router.post('/channels', upload.single('avatar'), Auth.auth([1]), ChannelController.create);
router.get('/channels/:id', ChannelController.details);
router.delete('/channels/:id', Auth.auth([1]), ChannelController.delete);
router.put('/channels/:id', upload.single('avatar'), Auth.auth([1]), ChannelController.update);

router.get('/channels/admins/:id', Auth.auth([1]), ChannelController.listAdmins);
router.put('/channels/admins/:id', Auth.auth([1]), ChannelController.adminAdd);
router.delete('/channels/admins/:id', Auth.auth([1]), ChannelController.adminDelete);

router.get('/channels/subscribers/:id', Auth.auth([1]), ChannelController.listSubsribers);
router.put('/channels/subscribers/:id', Auth.auth([1]), ChannelController.subscriberAdd);
router.delete('/channels/subscribers/:id', Auth.auth([1]), ChannelController.subscriberDelete);

router.get('/search/:keyword', SearchController.search);


router.get('/posts', PostController.list);
router.post('/posts',upload.single('image'), Auth.auth([1]), PostController.create);
router.get('/posts/:id', PostController.details);
router.delete('/posts/:id', Auth.auth([1]), PostController.delete);
router.put('/posts/:id', upload.single('image'), Auth.auth([1]), PostController.update);
router.put('/posts/vote/:id', Auth.auth([1]), PostController.addVote);


router.get('/comments', CommentController.list);
router.post('/comments', Auth.auth([1]), CommentController.create);
router.get('/comments/:id', CommentController.details);
router.delete('/comments/:id', Auth.auth([1]), CommentController.delete);
router.put('/comments/:id', Auth.auth([1]), CommentController.update);
router.put('/comments/like/:id', Auth.auth([1]), CommentController.addLike);

router.get('/reports',Auth.auth([10]), ReportController.list);
router.post('/reports', Auth.auth([1]), ReportController.create);
router.delete('/reports/:id', Auth.auth([10]), ReportController.delete);



export default router;