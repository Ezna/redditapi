import { Schema, model, Mongoose } from 'mongoose';
import Comment from "../models/Comment";

// Création du schema

const postSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    vote: {
        type: Number,
        default: 0
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    channelId: {
        type: Schema.Types.ObjectId,
        ref: 'Channel'
    },
    image: {
        type: String
    }
});


const Post = model('Post', postSchema);

export default Post;
