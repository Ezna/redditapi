import Post from "../models/Post";
import Comment from "../models/Comment";
import User from "../models/User";
import Channel from "../models/Channel";

class PostController{

    /**
     * Create post in database
     * @param {*} req 
     * @param {*} res 
     */
    static async create(req, res){
        let status = 200;
        let body = {};

        try{
            if(req.body.slug){
                let channel = await Channel.findOne({slug: req.body.slug});
                req.body.channelId = channel._id;
            }

            let post = await Post.create({
               title: req.body.title,
               content: req.body.content,
               userId: req.body.userId,
               channelId: req.body.channelId,
               image: req.file && `uploads/${req.file.filename}`,
            });

            body = {
                post,
                'message': 'Post created'
            };

        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }

        return res.status(status).json(body);
    }
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let posts = await Post.find().populate('userId');

            body = {
                posts,
                'message' : 'Post list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }


    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let id = req.params.id;
            let post = await Post.findById(id).populate('channelId').populate('userId');

            let comments = await Comment.find({postId: id}).populate('userId').populate('commentId');
            body = {
                post,
                comments,
                'message' : 'Post list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try{
            await Post.deleteOne({_id : req.params.id});
            body = {
                'message' : 'Post deleted'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            if(req.file){
                req.body.image = `uploads/${req.file.filename}`
            }
            await Post.findByIdAndUpdate(req.params.id, {$set : req.body});

            body = {
                'id':req.params.id,
                'body': req.body,
                'message' : 'Post updated'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    // Ajout d'un vote
    static async addVote(req, res){
        let status = 200;
        let body = {};

        try{

            await Post.findByIdAndUpdate(req.params.id, {$inc : {'vote' : req.body.vote}});



            if(req.body.vote < 0){
                await User.findByIdAndUpdate(req.body.userId, { $pull: { 'votes': req.params.id } });
            }else{
                await User.findByIdAndUpdate(req.body.userId, { $push: { 'votes': req.params.id } });
            }

            let post = await Post.findById(req.params.id);

            body = {
                post,
                'message' : 'Vote added'
            }
        }catch(error){
            status = 500;
            body = {
                'message' : error.message
            };
        }
        

        return res.status(status).json(body);
    }

    
}


export default PostController;