import { Schema, model, Mongoose } from 'mongoose';

// Création du schema

const reportSchema = new Schema({
    reporterId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    postId: {
        type: Schema.Types.ObjectId,
        ref: 'Post'
    },
    date: {
        type: Date,
        default: Date.now
    }
});


const Report = model('Report', reportSchema);
export default Report;
