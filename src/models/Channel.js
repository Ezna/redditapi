import { Schema, model, Mongoose } from 'mongoose';
import Post from "./Post";

// Création du schema

const channelSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    date: {
        type: Date,
        default: Date.now
    },
    slug: {
        type: String
    },
    avatar: {
        type: String,
        default: 'uploads/default-avatar.png'
    },
    ownerId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    adminsId: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    subscribersId: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]
});


channelSchema.pre('deleteOne', async function (next) {

    await Post.remove({ channelId: this.getQuery()._id});
    next();
});

const Channel = model('Channel', channelSchema);

export default Channel;
