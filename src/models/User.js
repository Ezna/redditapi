import { Schema, model } from 'mongoose';

const userSchema = new Schema({
    nickname: {
        type: String,
        required: true,
        unique: true,
    },
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    avatar: {
        type: String,
        default: 'uploads/default-avatar.png'
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
    },
    user_role: {
        type: Number,
        default: 1
    },
    banned: {
        type: Boolean,
        default: false
    },
    likes:[{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    votes:[{
        type: Schema.Types.ObjectId,
        ref: 'Post'
    }]
});


const User = model('User', userSchema);
export default User;