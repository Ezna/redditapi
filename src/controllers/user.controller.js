import User from "../models/User";
import Post from "../models/Post";
import Comment from "../models/Comment";
import jwt from "jsonwebtoken";
import Channel from "../models/Channel";

class UserController{

    /**
     * Create user in database
     * @param {*} req 
     * @param {*} res 
     */
    static async create(req, res){
        let status = 200;
        let body = {};


        try{

            let user = await User.create({
               nickname: req.body.nickname,
               firstname: req.body.firstname,
               lastname: req.body.lastname,
               description: req.body.description,
               avatar: req.file && `uploads/${req.file.filename}`,
               email: req.body.email,
               password: req.body.password
            });

            body = {
                user,
                'message': 'User created'
            };

        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }

        return res.status(status).json(body);
    }
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let users = await User.find();

            body = {
                users,
                'message' : 'User list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }


    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let id = req.params.id;
            let user = await User.findById(id);

            let posts = await Post.find({userId: id}).populate('userId channelId');
            let comments = await Comment.find({userId: id}).populate({path: 'postId',populate : {path:'channelId',model:'Channel'}}).populate('userId');

            body = {
                user,
                posts,
                comments,
                'message' : 'User list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try{
            await User.findByIdAndDelete(req.params.id);
            body = {
                'message' : 'User deleted'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        if(req.file){
            req.body.avatar = `uploads/${req.file.filename}`
        }

        try{
            await User.findByIdAndUpdate(req.params.id, {$set : req.body});
            let user = await User.findById(req.params.id);

            body = {
                user,
                'message' : 'User updated'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }


    static async auth(req, res){
        let status = 200;
        let body = {};
        
        try{
            let user = await User.findOne({email: req.body.email});

            
            if(user && user.password === req.body.password){
                let token = jwt.sign({
                    sub: user._id
                }, "monsecret");
                body = {
                    user,
                    token,
                    'message' : 'User loged'
                }
            }else{
                
                status = 401;
                body = {
                    'message' : 'Error email / password'
                }
            }
            
            

            
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }


    // Ban ou unban d'un utilisateur
    static async ban(req, res){
        let status = 200;
        let body = {};

        try{
            await User.findByIdAndUpdate(req.params.id, {banned : req.body.banned});
            let user = await User.findById(req.params.id);

            let banned = (req.body.banned === 'true') ? 'banned' : 'unbanned';
            body = {
                user,
                'message' : `User ${banned}`
            }
        }catch(error){
            status = 500;
            body = {
                'message' : error.message
            };
        }
        

        return res.status(status).json(body);
    }

    // Liste des abonnements de l'utilisateur
    static async listSubscriptions(req, res){
        let status = 200;
        let body = {};

        try{
            let channels = await Channel.find({subscribersId : req.params.id});

            body = {
                channels,
                'message' : 'User list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    
}


export default UserController;