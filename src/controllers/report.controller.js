import Report from "../models/Report";
import Post from "../models/Post";
import User from "../models/User";

class ReportController{

    /**
     * Create report in database
     * @param {*} req 
     * @param {*} res 
     */
    static async create(req, res){
        let status = 200;
        let body = {};


        try{
            let report = await Report.create({
               reporterId: req.body.reporterId,
               userId: req.body.userId,
               postId: req.body.postId,
            });

            body = {
                report,
                'message': 'Report created'
            };

        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }

        return res.status(status).json(body);
    }
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let reports = await Report.find().sort('-date').populate('reporterId userId postId');

            body = {
                reports,
                'message' : 'Report list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try{
            await Report.findByIdAndDelete(req.params.id);
            body = {
                'message' : 'Channel deleted'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }
    
}


export default ReportController;