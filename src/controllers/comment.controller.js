import Comment from "../models/Comment";
import Post from "../models/Post";
import User from "../models/User";

class CommentController{

    /**
     * Create comment in database
     * @param {*} req 
     * @param {*} res 
     */
    static async create(req, res){
        let status = 200;
        let body = {};

        
        try{
            let comment = await Comment.create({
               content: req.body.content,
               userId: req.body.userId,
               postId: req.body.postId,
               commentId: req.body.commentId
            
            });
            comment = await comment.populate('userId').execPopulate();
            
            body = {
                comment,
                'message': 'Comment created'
            };

        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }

        return res.status(status).json(body);
    }

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let comments = await Comment.find().populate('userId postId');

            body = {
                comments,
                'message' : 'Comment list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }


    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let id = req.params.id;
            let comment = await Comment.findById(id);

            let comments = await Comment.find({commentId: id}).populate('userId').populate('commentId');

            body = {
                comment,
                comments,
                'message' : 'Comment list'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try{
            await Comment.deleteOne({_id : req.params.id});
            body = {
                'message' : 'Comment deleted'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            await Comment.findByIdAndUpdate(req.params.id, {$set : req.body});

            body = {
                'message' : 'Comment updated'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    // Ajout d'un j'aime
    static async addLike(req, res){
        let status = 200;
        let body = {};

        try{

            await Comment.findByIdAndUpdate(req.params.id, {$inc : {'like' : req.body.like}});

            if(req.body.like < 0){
                await User.findByIdAndUpdate(req.body.userId, { $pull: { 'likes': req.params.id } });
            }else{
                await User.findByIdAndUpdate(req.body.userId, { $push: { 'likes': req.params.id } });
            }
            
            let comment = await Comment.findById(req.params.id);

            body = {
                comment,
                'message' : 'Like added'
            }
        }catch(error){
            status = 500;
            body = {'message' : error.message};
        }
        

        return res.status(status).json(body);
    }

    
}


export default CommentController;